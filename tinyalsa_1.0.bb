inherit autotools pkgconfig

DESCRIPTION = "Tinyalsa Library"
LICENSE = "BSD-3-Clause-Clear"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta-qti-bsp/files/common-licenses/\
${LICENSE};md5=3771d4920bd6cdb8cbdf1e8344489ee0"
FILESPATH =+ "${WORKSPACE}/:"
SRC_URI = "file://vendor/qcom/opensource/tinyalsa"
SRC_URI += "file://${BASEMACHINE}/"

S = "${WORKDIR}/vendor/qcom/opensource/tinyalsa"
PR = "r0"

DEPENDS = "libcutils glib-2.0 "

EXTRA_OECONF += "--with-glib"


SOLIBS = ".so"

FILES_SOLIBSDEV = ""
